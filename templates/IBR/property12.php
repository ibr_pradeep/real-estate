<!DOCTYPE html>


<html lang="{lang_code}">

  <head>

    {template_head}
    
        

    <?php if(file_exists(FCPATH.'templates/'.$settings_template.'/assets/js/places.js')): ?>

    <script src="assets/js/places.js"></script>

    <?php endif; ?>
    
      </head>

     <!--<h2>{lang_Bookingform}<?php echo('property/'.$estate->id.'/'.$lang_code.'/'.$estate->address); ?></h2>-->
     <h2>boekingsformulier</h2>

                  <div id="form" class="property-form">

                    {validation_errors}

                    {form_sent_message} 
					<?php $ami= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
                    <form method="post" action="{page_current_url}#form">
							<input type="hidden" name="flag_ami"  value="Reserveren" />				
                        <label>{lang_FirstLast}</label>
						
                        <input class="{form_error_firstname}" name="firstname" type="text" placeholder="{lang_FirstLast}" value="{form_value_firstname}" />

                        <label>{lang_Phone}</label>

                        <input class="{form_error_phone}" name="phone" type="text" placeholder="{lang_Phone}" value="{form_value_phone}" />

                        <label>{lang_Email}</label>

                        <input class="{form_error_email}" name="email" type="text" placeholder="{lang_Email}" value="{form_value_email}" />
                        
						

                        <?php /*?><label>{lang_Address}</label>

                        <input class="{form_error_address}" name="address" type="text" placeholder="{lang_Address}" value="{form_value_address}" /><?php */?>

                        

                        <label>{lang_FromDate}</label>

                        <input name="fromdate" type="text" id="datetimepicker1" value="{form_value_fromdate}" class="{form_error_fromdate}" placeholder="{lang_FromDate}" />

                        <label>{lang_ToDate}</label>

                        <input class="{form_error_todate}" id="datetimepicker2" name="todate" type="text" placeholder="{lang_ToDate}" value="{form_value_todate}" />

                        

                        <label>{lang_Message}</label>

                        <textarea class="{form_error_message}" name="message" rows="3" placeholder="{lang_Message}">{form_value_message}</textarea>

                        

                        <?php if(config_item('captcha_disabled') === FALSE): ?>

                        <label class="captcha"><?php echo $captcha['image']; ?></label>

                        <input class="captcha {form_error_captcha}" name="captcha" type="text" placeholder="{lang_Captcha}" value="" />

                        <br style="clear: both;" />

                        <input class="hidden" name="captcha_hash" type="text" value="<?php echo $captcha_hash; ?>" />

                        <?php endif; ?>



                        <br style="clear: both;" />

                        <p style="text-align:right;">

                    <!-- <button type="submit" class="btn btn-info">{lang_CalculateBook}</button>-->
                        <button type="submit" class="btn btn-info">Bereken & Boek</button>

                        </p>

                    </form>

                  </div>

</html>