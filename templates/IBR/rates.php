
<style>
.fancybox-wrap,
.fancybox-skin,
.fancybox-outer,
.fancybox-inner,
.fancybox-image,
.fancybox-wrap iframe,
.fancybox-wrap object,
.fancybox-nav,
.fancybox-nav span,
.fancybox-tmp
{
	padding: 0;
	margin: 0;
	border: 0;
	outline: none;
	vertical-align: top;
	height:300px !important;
}
.fancybox-iframe {
	display: block;
	width: 100%;
	height:300px !important;
}
.fancybox-nav {
	position: absolute;
	top: 0;
	width: 40%;
	height:300px !important;
	cursor: pointer;
	text-decoration: none;
	background: transparent url('blank.gif'); /* helps IE */
	-webkit-tap-highlight-color: rgba(0,0,0,0);
	z-index: 8040;
}

iframe{ height:250px !important}
</style>
<!DOCTYPE html>
<html lang="{lang_code}">
  <head>
  <script src='assets/rateAssets/jquery.js' type="text/javascript"></script>
	<script src='assets/rateAssets/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
 <script src='assets/rateAssets/jquery.rating.js' type="text/javascript" language="javascript"></script>
 <link href='assets/rateAssets/jquery.rating.css' type="text/css" rel="stylesheet"/>
 <!--// documentation resources //-->
 <!--<script src="http://code.jquery.com/jquery-migrate-1.1.1.js" type="text/javascript"></script>-->

 <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
<!-- <link href='documentation/documentation.css' type="text/css" rel="stylesheet"/>
	<script src='documentation/documentation.js' type="text/javascript"></script> -->
    
  </head>

  <body>
  
  

  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="assets/css/styles.css" rel="stylesheet">
    
        <link href="assets/css/enable-responsive.css" rel="stylesheet">
  <!--

<script src='assets/rateAssets/jquery.js' type="text/javascript"></script>
<script src='assets/rateAssets/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
 <script src='assets/rateAssets/jquery.rating.js' type="text/javascript" language="javascript"></script>
 <link href='assets/rateAssets/jquery.rating.css' type="text/css" rel="stylesheet"/>
-->
<script type="text/javascript" language="javascript">
$(function(){ 
 $('#form1 :radio.star').rating(); 
 $('#form2 :radio.star').rating({cancel: 'Cancel', cancelValue: '0'}); 
 $('#form3 :radio.star').rating(); 
 $('#form4 :radio.star').rating(); 
});
</script>
<script type="text/javascript">
$(function(){
 $('#tab-Testing form').submit(function(){
  $('.test',this).html('');
  $('input',this).each(function(){
   if(this.checked) $('.test',this.form).append(''+this.name+': '+this.value+'<br/>');
		});
  return false;
 });
});
</script>
	<div id="form" class="property-form">
             <h4><?php echo  lang('Rate_Property') ;  ?></h4>
             <!--action="<?php echo base_url()?>home.php/frontend/saveRatings"-->
            <form method="post" >
            <span id="message" style="color:green"></span>
            <input type="hidden" name="property_id" id="property_id" value="<?php echo isset($property_id['id'])?$property_id['id']:'' ; ?>" >
             <label> <?php  echo lang('Rating'); ?> </label>
						
  <div class="Clear">
    
    <input class="star" type="radio" name="test-1-rating-4" value="1" id="rate1" title="Worst"/>
    <input class="star" type="radio" name="test-1-rating-4" value="2" id="rate2" title="Bad"/>
    <input class="star" type="radio" name="test-1-rating-4" value="3" id="rate3" title="OK"/>
    <input class="star" type="radio" name="test-1-rating-4" value="4" id="rate4" title="Good"/>
    <input class="star" type="radio" name="test-1-rating-4" value="5" id="rate5" title="Best"/>
   </div>
<br>
                        <label><?php echo lang('Comment');?></label>

                        <textarea name="comment" id="comment" rows="5" ></textarea>
                     
                      <button type="button" onClick="saverating();" class="btn btn-info pull-right"><?php echo lang('Submit') ?></button>
            </form>
            </div>



<!-- The Gallery as lightbox dialog, should be a child element of the document body -->


  </body>
</html>
<script type="text/javascript">
$(function(){
 $('.hover-star').rating({
  focus: function(value, link){
    // 'this' is the hidden form element holding the current value
    // 'value' is the value selected
    // 'element' points to the link element that received the click.
    var tip = $('#hover-test');
    tip[0].data = tip[0].data || tip.html();
    tip.html(link.title || 'value: '+value);
  },
  blur: function(value, link){
    var tip = $('#hover-test');
    $('#hover-test').html(tip[0].data || '');
  }
 });
});
</script>
<script type="text/javascript">

function saverating(){

	 var comment = $("#comment").val();
	 var property_id = $("#property_id").val();
	  
	  if($('.star').is(':checked')) {  var rates = $("input[name='test-1-rating-4']:checked").val();}
	  $.ajax({
				url: '<?php echo base_url()?>home.php/frontend/saveRatings',
				type: 'post',
				data : {rates: rates ,comment:comment ,property_id:property_id },
				success: function(json) {
						var result_obj = JSON.parse(json);
						$("#comment").val(result_obj['comment'])  ;
						$("#message").html("Your rating post sucessfully..!!")  ;
						
				}
	   });
 
 }
</script>
<script type="text/javascript">
$(function(){
 $('.hover-star').rating({
  focus: function(value, link){
    // 'this' is the hidden form element holding the current value
    // 'value' is the value selected
    // 'element' points to the link element that received the click.
    var tip = $('#hover-test');
    tip[0].data = tip[0].data || tip.html();
    tip.html(link.title || 'value: '+value);
  },
  blur: function(value, link){
    var tip = $('#hover-test');
    $('#hover-test').html(tip[0].data || '');
  }
 });
});
</script>

        <script src="assets/js/jquery.textareaCounter.plugin.js" type="text/javascript"></script>
        <script type="text/javascript">
			var info;
			$(document).ready(function(){
				
				var options3 = {
						'maxCharacterSize': 75,
						'originalStyle': 'originalTextareaInfo',
						'warningStyle' : 'warningTextareaInfo',
						'warningNumber': 70,
						'displayFormat' : '#left Characters Left / #max'
				};
				$('#comment').textareaCount(options3, function(data){
					$('#showData').html(data.input + " characters input. <br />" + data.left + " characters left. <br />" + data.max + " max characters. <br />" + data.words + " words input.");
				});
			});
		</script>