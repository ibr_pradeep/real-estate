<?php 

// App types: demo, cms
$config['app_type'] = 'cms';

// estates pagination
$config['per_page'] = 8;

$config['default_template_css'] = 'assets/css/bootstrap.min.css';

$config['installed'] = TRUE;

//$config['listing_uri'] = 'listing';

//$config['color'] = 'purple';

$config['color_picker'] = TRUE;

$config['def_package'] = 1;

$config['captcha_disabled'] = FALSE;

$config['show_not_available_amenities'] = FALSE;

$config['ad_gallery_enabled'] = FALSE;

$config['all_results_default'] = TRUE;

$config['per_page_agents'] = 6;

//$config['cookie_warning_enabled'] = TRUE;

//$config['custom_map_center'] = '27.175015, 78.042155';

//$config['reactivation_enabled'] = TRUE;

$config['codecanyon_username'] = 'sanljiljan';
$config['codecanyon_code'] = 'sanljiljan';

//$config['disable_responsive'] = TRUE;

$config['admin_beginner_enabled'] = TRUE;

//$config['agent_masking_enabled'] = TRUE;

$config['price_by_purpose'] = TRUE;
