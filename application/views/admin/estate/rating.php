<div class="page-head">
    <!-- Page heading -->
      <h2 class="pull-left"><?php echo lang('manage_rating')?>
      <!-- page meta -->
      <span class="page-meta"><?php echo lang('View_all_rates')?></span>
    </h2>
    
    
    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
      <a href="<?php echo site_url('admin')?>"><i class="icon-home"></i> <?php echo lang('Home')?></a> 
      <!-- Divider -->
      <span class="divider">/</span> 
      <a class="bread-current" href="<?php echo site_url('admin/estate')?>"><?php echo lang('Estates')?></a>
    </div>
    
    <div class="clearfix"></div>
</div>

<div class="matter">
        <div class="container">
        
        <div class="row">
            <div class="col-md-12"> 
               
            </div>
        </div>

          <div class="row">

            <div class="col-md-12">

                <div class="widget wlightblue">

                <div class="widget-head">
                  <div class="pull-left"><?php echo lang('ratings')?></div>
                  <div class="widget-icons pull-right">
                    <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a> 
                  </div>
                  <div class="clearfix"></div>
                </div>

                  <div class="widget-content">
                    <?php if($this->session->flashdata('error')):?>
                    <p class="label label-important validation"><?php echo $this->session->flashdata('error')?></p>
                    <?php endif;?>
                    <table class="table table-bordered ami">
                      <thead>
                        <tr>
                        	<th>#</th>
                            <th><?php echo lang('property_name');?></th>
                           
                            <!-- Dynamic generated -->
                          
                       		  <th class="control"><?php echo lang('ratings');?></th>
                         
                        	<th class="control"><?php echo lang('Edit');?></th>
                        	<?php if(check_acl('estate/delete')):?><th class="control"><?php echo lang('Delete');?></th><?php endif;?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(count($rates)): foreach($rates as $rate):?>
                                    <tr>
                                    	<td><?php echo $rate->id ?></td>
                                        
                                        <td>
                                      	<?php echo $this->method_call->property_name($rate->property_id ) ; ?>		
                                        </td>
                                      	
                                        <!-- Dynamic generated -->
                                      
                                        <!-- End dynamic generated -->
                                     	<td><?php echo $rate->rate?></td>
                                    	<td><?php echo btn_edit('admin/rating/edit/'.$rate->id)?></td>
                                    	<?php if(check_acl('estate/delete')):?><td><?php echo btn_delete('admin/rating/delete/'.$rate->id)?></td><?php endif;?>
                                    </tr>
                        <?php endforeach;?>
                        <?php else:?>
                                    <tr>
                                    	<td colspan="20"><?php echo lang('We could not find any');?></td>
                                    </tr>
                        <?php endif;?>           
                      </tbody>
                    </table>
                    
                    <div style="text-align: center;"><?php echo $pagination; ?></div>

                  </div>
                </div>
            </div>
          </div>
        </div>
</div>
    
    
    
    
    
</section>