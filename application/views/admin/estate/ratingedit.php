<script type="text/javascript">

$(document).ready(function(){
	$('#inputOption_1_32').each(function () {
		//alert('dddd');
  $(this).children('option:first').attr('value','');
});
});


</script>
<div class="page-head">
    <!-- Page heading -->
      <h2 class="pull-left"><?php echo lang('ratings')?>
          <!-- page meta -->
          <span class="page-meta"><?php echo empty($estate->id) ? lang('Add a ratings') : lang('Edit estate').' "' . $estate->id.'"'?></span>
        </h2>
    
    
    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
      <a href="<?php echo site_url('admin')?>"><i class="icon-home"></i> <?php echo lang('Home')?></a> 
      <!-- Divider -->
      <span class="divider">/</span> 
      <a class="bread-current" href="<?php echo site_url('admin/rating/edit/')?>"><?php echo lang('Rating_Data')?></a>
    </div>
    
    <div class="clearfix"></div>

</div>

<div class="matter">
        <div class="container">
        
            <div class="row">
                <div class="col-md-12" style="text-align:right;"> 
                <?php if(!empty($estate->id) && file_exists(APPPATH.'controllers/admin/reviews.php') && check_acl('reviews')): ?>
                    <?php echo anchor('admin/reviews/index/'.$estate->id, '<i class="icon-star"></i>&nbsp;&nbsp;'.lang('Reviews'), 'class="btn btn-primary"')?>
                <?php endif; ?>
                </div>
            </div>

          <div class="row">

            <div class="col-md-8">
              <div class="widget wlightblue">
                
                <div class="widget-head">
                  <div class="pull-left"><?php echo lang('Rating_Data')?></div>
                  <div class="widget-icons pull-right">
                    <a class="wminimize" href="#"><i class="icon-chevron-up"></i></a> 
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">
                    <?php echo validation_errors()?>
                    <?php if($this->session->flashdata('message')):?>
                    <?php echo $this->session->flashdata('message')?>
                    <?php endif;?>
                    <?php if($this->session->flashdata('error')):?>
                    <p class="label label-important validation"><?php echo $this->session->flashdata('error')?></p>
                    <?php endif;?>
                    <hr />
                    <!-- Form starts.  -->
                    <?php echo form_open(NULL, array('class' => 'form-horizontal form-estate', 'role'=>'form'))?>                              
                                
                                   <input type="hidden" name="rating_id" id="rating_id" value="<?php echo $row['id']; ?>"  />
                              	<div class="form-group">
                                  <label class="col-lg-3 control-label"><?php echo lang('property_name')?></label>
                                  <div class="col-lg-9">
                                 
								     <?php 
									 
									$property_name =  $this->method_call->property_name($row['property_id']  ) ;  
									 echo form_input('property_name', set_value('property_name', $property_name), 'class="form-control" id="property_name" placeholder="'.lang('property_name').'"')?>
								
                                 
                                  </div>
                                </div>
                              
                                 <div class="form-group">
                                  <label class="col-lg-3 control-label"><?php echo lang('ratings')?></label>
                                  <div class="col-lg-9">
                                  <?php 
								  	$rating_option = array(1=>1,2=>2,3=>3,4=>4,5=>5);
								  ?>
                                   <?php echo form_dropdown('rates', $rating_option, set_value('ratings', $row['rate']), 'class="form-control" id="ratings" placeholder="'.lang('ratings').'"')?>
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <label class="col-lg-3 control-label"><?php echo lang('comment')?></label>
                                  <div class="col-lg-9">
                                   <?php echo form_textarea('comment', set_value('comment', isset($row['comment'])?$row['comment']:''), 'class=" form-control" id="comment" ')?>
                                   
                                  </div>
                                </div>
                                
                               
                                
                                
                                <div class="form-group">
                                  <div class="col-lg-offset-3 col-lg-9">
                                    <?php echo form_submit('submit', lang('Save'), 'class="btn btn-primary"')?>
                                    <a href="<?php echo site_url('admin/rating')?>" class="btn btn-default" type="button"><?php echo lang('Cancel')?></a>
                                  </div>
                                </div>
                       <?php echo form_close()?>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
              </div>  

            </div>
            
            
            
            

            
            
            
            
            
            
            
            
            

          </div>
          
          
          
            </div>
		  </div>
          
          <script>
		  function getvalue(val)
		  {
				
				$("#inputOption_1_5").val(val);		 
		 }
		 function getaddressval(str){
			
			 $("#inputAddress").val(str);
		 }
		  </script>