<?php

class Rating extends Admin_Controller
{
    
	public function __construct()
    {
		parent::__construct();
        $this->load->model('estate_m');
        $this->load->model('option_m');
        $this->load->model('file_m');
		$this->method_call =& get_instance();
        // Get language for content id to show in administration
        $this->data['content_language_id'] = $this->language_m->get_content_lang();
	}
    
    public function index($pagination_offset=0)
	{
		
	    $this->load->library('pagination');
        
        // Fetch all estates
       	$this->data['rates'] = $this->estate_m->get_rates();
     //   $this->data['languages'] = $this->language_m->get_form_dropdown('language');
     //   $this->data['options'] = $this->option_m->get_rating($this->data['content_language_id']);
    //    $this->data['available_agent'] = $this->user_m->get_form_dropdown('name_surname'/*, array('type'=>'AGENT')*/);

        $config['base_url'] = site_url('admin/rating/index');
        $config['uri_segment'] = 4;
        $config['total_rows'] = count($this->data['rates']);
        $config['per_page'] = 20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        
        $this->data['rates'] = $this->estate_m->get_rates($config['per_page'], $pagination_offset);
        
        // Load view
		$this->data['subview'] = 'admin/estate/rating';
        $this->load->view('admin/_layout_main', $this->data);
	}
    
    public function edit($id = NULL)
	{
		
		if($this->input->post('submit')){
			
			$data['rate'] = $this->input->post('rates');	
			$rating_id = $this->input->post('rating_id');
			$this->db->where('id',$rating_id);
			$this->db->update('user_rates',$data);
		}
		$query = $this->db->get_where('user_rates',array('id'=>$id));
		$this->data['row'] = $query->row_array();
		$this->data['subview'] = 'admin/estate/ratingedit';
        $this->load->view('admin/_layout_main', $this->data);
			
	}
    
    public function values_correction(&$str)
    {
        $str = str_replace(', ', ',', $str);
        
        return TRUE;
    }
    
    public function values_dropdown_check($str)
    {
        static $already_set = false;
        $comma_count = -1;
        
        if($already_set == true)
            return TRUE;
        
        foreach($this->option_m->languages as $key=>$value)
        {
            $values_post = $this->input->post("values_$key");
            
            $comma_cur_count = substr_count($values_post, ',');
            
            if($comma_count == -1)$comma_count = $comma_cur_count;
            
            if($comma_count != $comma_cur_count)
            {
                $this->form_validation->set_message('values_dropdown_check', lang_check('Values number must be same in all languages'));
                $already_set = true;
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
	public function gps_check($str)
	{
        $gps_coor = explode(', ', $str);
        
        if(count($gps_coor) != 2)
        {
        	$this->form_validation->set_message('gps_check', lang_check('Please check GPS coordinates'));
        	return FALSE;
        }
        
        if(!is_numeric($gps_coor[0]) || !is_numeric($gps_coor[1]))
        {
        	$this->form_validation->set_message('gps_check', lang_check('Please check GPS coordinates'));
        	return FALSE;
        }
        
        return TRUE;
	}
    
    public function delete($id)
	{
        $this->db->where('id',$id);
		$this->db->delete('user_rates');	
		
        redirect('admin/rating');
	}
    
    public function options()
	{
        // Fetch all estates
        $this->data['languages'] = $this->language_m->get_form_dropdown('language');
        $this->data['options_no_parents'] = $this->option_m->get_no_parents($this->data['content_language_id']);
        $this->data['options'] = $this->option_m->get_lang(NULL, FALSE, $this->data['content_language_id']);
        $this->data['options_nested'] = $this->option_m->get_nested($this->data['content_language_id']);
        
        //var_dump($this->data['options_nested']);
        
        // Load view
		$this->data['subview'] = 'admin/estate/options';
        $this->load->view('admin/_layout_main', $this->data);
	}
    
    public function edit_option($id = NULL)
	{
	    // Fetch a record or set a new one
	    if($id)
        {
            $this->data['option'] = $this->option_m->get_lang($id, FALSE, $this->data['content_language_id']);
            count($this->data['option']) || $this->data['errors'][] = 'Could not be found';
        }
        else
        {
            $this->data['option'] = $this->option_m->get_new();
        }
        
		// Options for dropdown
        $this->data['options_no_parents'] = $this->option_m->get_no_parents($this->data['content_language_id']);
        $this->data['languages'] = $this->language_m->get_form_dropdown('language');

        // Set up the form
        $rules = $this->option_m->get_all_rules();
        $this->form_validation->set_rules($rules);

        // Process the form
        if($this->form_validation->run() == TRUE)
        {
            if($this->config->item('app_type') == 'demo')
            {
                $this->session->set_flashdata('error', 
                        lang('Data editing disabled in demo'));
                redirect('admin/estate/edit_option/'.$id);
                exit();
            }
            
            $data = $this->option_m->array_from_post($this->option_m->get_post_fields());
            if($id == NULL)
            {
                //get max order in parent id and set
                $parent_id = $this->input->post('parent_id');
                $data['order'] = $this->option_m->max_order($parent_id);
            }
            
            $data_lang = $this->option_m->array_from_post($this->option_m->get_lang_post_fields());
            $this->option_m->save_with_lang($data, $data_lang, $id);
            
            //$this->output->enable_profiler(TRUE);
            redirect('admin/estate/options');
        }
        
        // Load the view
		$this->data['subview'] = 'admin/estate/edit_option';
        $this->load->view('admin/_layout_main', $this->data);
	}
    
   
    
   
 	public function property_name($property_id )
	{
		$query = $this->db->get_where('property_value',array('property_id'=>$property_id , 'option_id'=>10));
		$rows = $query->row_array();
		
		return $rows['value'];
	}
}